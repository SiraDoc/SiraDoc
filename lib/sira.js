#!/usr/bin/env node
/**
 * $File: sira.js $
 * $Date: 2021-08-28 23:16:25 $
 * $Revision: $
 * $Creator: Jen-Chieh Shen $
 * $Notice: See LICENSE.txt for modification and distribution information
 *                   Copyright © 2021 by Shen, Jen-Chieh $
 */

"use strict";

/*
 * Dependencies
 */
const fs = require("fs");
const path = require('path');

const fse = require('fs-extra');
const dirTree = require('directory-tree');
const replaceExt = require('replace-ext');

const HTMLParser = require('node-html-parser');
const showdown = require('showdown');
const showdownHighlight = require("showdown-highlight");

const minify = require('minify');
const walk = require('walkdir');

const yaml = require('js-yaml');

const util = require('./util');
const custom = require('./custom');

/* Global Variables */
var layerNum = -1;
var config = null;

/*
 * Core
 */

/* Create index with directory. */
function createIndexWithDir(dir, inParent) {
  let ul = HTMLParser.parse('<ul></ul>');
  inParent.appendChild(ul);

  let parent = inParent.querySelector('ul');

  ++layerNum;

  let currentDir = "";

  for (let index = 0; index < dir.length; ++index) {
    let pathObj = dir[index];

    if (pathObj.path.charAt(0) != "/")
      currentDir = pathObj.path;

    let sbType = "sb-dir";
    if (pathObj.type == "file") {
      sbType = "sb-file";
    }

    let isDir = (pathObj.type != "file");

    let newPath = pathObj.path;
    newPath = newPath.substring(1);  // remove first slash in path
    if (!isDir) newPath = newPath.replace(/\.[^/.]+$/, "");  // Remove extension if file.

    /* IMPORTANT: Apply conversion rule. */
    newPath = util.applyConversionRule(newPath);

    let dirOrFileName = pathObj.name;
    dirOrFileName = dirOrFileName.replace(/\.[^/.]+$/, "");  // Remove extension if there is.

    let li = HTMLParser.parse("<li class=" + sbType +" id=" + newPath + "></li>");
    parent.appendChild(li);

    let newPathNode = parent.querySelector('#' + newPath);

    let htmlDirOrFileName = "<span>" + dirOrFileName + "</span>";
    if (isDir) {
      htmlDirOrFileName = "<div class='arrow'>+</div>" + htmlDirOrFileName;
    }

    let temp = HTMLParser.parse(htmlDirOrFileName);
    newPathNode.appendChild(temp);

    newPathNode.classList.add("sb-layer-" + layerNum);

    if (pathObj.children != null && pathObj.children.length != 0) {
      createIndexWithDir(pathObj.children, newPathNode);
    }
  }

  --layerNum;

  return inParent;
}

function readConfig(src, dest) {
  util.info('Reading the configuration file...');
  let pathConfigYaml = path.join(src, 'siradoc.yml');

  config = yaml.load(util.readFile(pathConfigYaml));
}

function prepareSite(src, dest) {
  util.info('Copying template to current directory...');
  let sitePath = path.join(__dirname, '..', 'site');
  fse.copySync(sitePath, dest);
}

function getDocTree(path, ext) {
  const tree = dirTree(path, { extensions: ext, normalizePath: true });
  if (tree === null)
    return null;
  sortTreeByType(tree.children, 'directory');
  var removePath = path;
  removePath = removePath.replace("./", "");
  removePath = removePath.replace(/\\/g, '/');
  removeLeadPath(tree.children, removePath);
  return tree;
}

/**
 * Convert markdown text to html text.
 * @param { string } markdown - Markdown text in string.
 */
function md2html(markdown) {
  showdown.setFlavor('github');
  let converter = new showdown.Converter({ extensions: [showdownHighlight({ pre: true })]});
  converter.setOption('simpleLineBreaks', false);
  return converter.makeHtml(markdown);
}

/**
 * Generate a HTML file from a Markdown file.
 * @param { string } src - A HTML file path.
 * @param { string } dest - A Markdown file path.
 */
function genHtmlFromMd(src, dest) {
  let markdown = util.readFile(src);
  let html = md2html(markdown);
  util.writeFile(dest, html);
}

/**
 * Build html files from a tree directory; this generates markdown files.
 * @param { JSON } tree - Tree directory object.
 * @param { string } src - Path to the html files. (path to a directory)
 * @param { string } dest - Where to output the markdown files.
 */
function buildHtmlFiles(tree, src, dest) {
  for (let index = 0; index < tree.length; ++index) {
    let pathObj = tree[index];
    if (pathObj.children != null && pathObj.children.length != 0) {
      buildHtmlFiles(pathObj.children, src, dest);
    }

    if (pathObj.type === 'file') {
      let destFile = path.join(dest, replaceExt(pathObj.path, '.html'));
      let destDir = path.dirname(destFile);
      util.mkdir(destDir);

      let srcFile = path.join(src, pathObj.path);
      genHtmlFromMd(srcFile, destFile);
    }
  }
}

/** Build intro pages. */
function buildIntro(src, dest) {
  util.info('Building intro pages...');

  let srcIntroManual = path.join(src, 'Manual.md');
  let srcIntroScriptRef = path.join(src, 'ScriptReference.md');

  let destIntroManual = path.join(dest, 'Manual', 'intro.html');
  let destIntroScriptRef = path.join(dest, 'ScriptReference', 'intro.html');

  genHtmlFromMd(srcIntroManual, destIntroManual);
  genHtmlFromMd(srcIntroScriptRef, destIntroScriptRef);
}

/** Build the entire documents directory. */
function buildDocs(src, dest) {
  util.info('Building documents directory...');

  let pathManual = path.join(src, 'Manual');
  let pathScriptRef = path.join(src, 'ScriptReference');
  let treeManual = getDocTree(pathManual, /\.md/);
  let treeScriptRef = getDocTree(pathScriptRef, /\.md/);

  let oPathManual = path.join(dest, 'Manual', 'doc');
  let oPathScriptRef = path.join(dest, 'ScriptReference', 'api');

  if (treeManual !== null)
    buildHtmlFiles(treeManual.children, pathManual, oPathManual);
  if (treeScriptRef !== null)
    buildHtmlFiles(treeScriptRef.children, pathScriptRef, oPathScriptRef);
}

/** Build the website's index for the navigation side bar. */
function buildIndex(indexPath, docPath) {
  let html = util.readFile(indexPath);
  let tree = getDocTree(docPath, /\.html/);

  let root = HTMLParser.parse(html);
  let indexPos = root.querySelector('#index-pos');

  let newPage = createIndexWithDir(tree.children, indexPos);
  indexPos.replaceWith(newPage);
  util.writeFile(indexPath, root.toString());
}

/** Build the entire navigation side bar. */
function buildSidebar(src, dest) {
  let indexManual = path.join(dest, 'Manual', 'index.html');
  let indexScriptRef = path.join(dest, 'ScriptReference', 'index.html');

  let oPathManual = path.join(dest, 'Manual', 'doc');
  let oPathScriptRef = path.join(dest, 'ScriptReference', 'api');

  buildIndex(indexManual, oPathManual);
  buildIndex(indexScriptRef, oPathScriptRef);
}

function buildSite(src, dest) {
  util.info('Generating static site...');

  buildSidebar(src, dest);  // build index links on the side
}

/** Copy media files, and correct the media path */
function buildMedia(src, dest) {
  util.info('Copying all media files over...');

  walk.sync(dest, function (file, state) {
    if (util.isDir(file))
      return;

    let ext = path.extname(file);
    if (ext === '.html') {
      let content = util.readFile(file);
      let root = HTMLParser.parse(content);
      let imgs = root.querySelectorAll('img');

      let fileDir = path.dirname(file);
      let base = path.basename(file);

      let shortenFile = file.replace(dest, '');
      let isRoot = (base === 'intro.html');
      let type = util.pathIn(shortenFile, 0);

      let writeIt = false;

      for (let index = 0; index < imgs.length; ++ index) {
        let img = imgs[index];
        let imgSrc = img.getAttribute('src');
        // Absolute? URL? Local?
        if (fs.existsSync(imgSrc) || imgSrc.startsWith('http') || imgSrc.startsWith('file:')) {
          continue;
        }

        let destDir = '';
        if (!isRoot) {
          if (type === 'Manual') destDir = 'doc';
          else if (type === 'ScriptReference') destDir = 'api';
        }

        let typeDir = path.join(dest, type);
        let destFile = path.join(typeDir, destDir);

        let dir = fileDir.replace(destFile, '');
        let srcFile = path.join(dir, imgSrc);  // convert to absolute path
        destFile = path.join(destFile, srcFile);
        let rootType = (isRoot) ? '': type;
        srcFile = path.join(src, rootType, srcFile);
        util.copyFile(srcFile, destFile);

        let newImgSrc = destFile.replace(typeDir, '');
        img.setAttribute('src', path.join('.', newImgSrc));
        writeIt = true;
      }

      if (writeIt) {
        util.writeFile(file, root.toString());  // Update the image file path
      }
    }
  });
}

/** Apply siradoc.yml to config.js */
function applyConfig(src, dest) {
  util.info('Applying the configuration to generated site...');

  let pathConfigJS = path.join(dest, 'js', 'config.js');
  let content = util.readFile(pathConfigJS);

  content = custom.applyConfigKeywords(config, content);

  util.writeFile(pathConfigJS, content);
}

/** Minify all the source file if possible */
function optimizeBuild(src, dest) {
  util.info('Optimizing generated website...');

  walk.sync(dest, function (file, state) {
    if (util.isDir(file))
      return;

    let ext = path.extname(file);
    if (util.optTarget(ext)) {
      minify(file, custom.options).then(function (data) { util.writeFile(file, data); });
    }
  });
}

/**
 * The CLI entry.
 */
function build(iPath, oPath) {
  let src = path.join(path.resolve(iPath), '/');
  let dest = path.join(path.resolve(oPath), '/');

  readConfig(src, dest);
  prepareSite(src, dest);  // copy template over
  buildIntro(src, dest);
  buildDocs(src, dest);
  buildSite(src, dest);
  buildMedia(src, dest);
  applyConfig(src, dest);
  optimizeBuild(src, dest);
}

/**
 * Sort the tree result by directory/file type.
 * @param { tree.children } tree : Tree children.
 * @param { string } type : Sort type, enter 'directory' or 'file'.
 */
function sortTreeByType(tree, type = 'directory') {
  if (type != 'directory' && type != 'file')
    return;

  let tarList = [];  // target list.
  let anoList = [];  // another list.

  /* Split path object into two arrays by type. */
  for (let index = 0; index < tree.length; ++index) {
    let pathObj = tree[index];
    if (pathObj.children != null && pathObj.children.length != 0) {
      sortTreeByType(pathObj.children, type);
    }

    // Add path object by type.
    if (pathObj.type == type)
      tarList.push(pathObj);
    else
      anoList.push(pathObj);
  }

  /* Copy array over. */
  let resultTree = tarList.concat(anoList);

  for (let index = 0; index < tree.length; ++index) {
    tree[index] = resultTree[index];
  }
}

/**
 * Remove the `MANUAL_DIR_PATH' or `API_DIR_PATH', so when the client
 * receive the data would not need to care where is the api directory located.
 * @param { JSON } dir : directory JSON object.
 * @param { typename } rmPath : Param desc here..
 */
function removeLeadPath(dir, rmPath) {
  for (let index = 0; index < dir.length; ++index) {
    let pathObj = dir[index];

    if (pathObj.children != null && pathObj.children.length != 0) {
      removeLeadPath(pathObj.children, rmPath);
    }

    // Remove the `MANUAL_DIR_PATH' or `API_DIR_PATH' path.
    pathObj.path = pathObj.path.replace(rmPath, "");
  }
}

/*
 * Module Exports
 */
module.exports.build = build;
